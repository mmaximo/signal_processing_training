function compareMonteCarloEKFUKF()

monteCarloEKF = load('../../ekf/matlab/monte_carlo.mat');
monteCarloUKF = load('monte_carlo.mat');

figure;
hold on;
plot(monteCarloEKF.monteCarlo.time, monteCarloEKF.monteCarlo.rmsePosition, 'LineWidth', 2);
plot(monteCarloUKF.monteCarlo.time, monteCarloUKF.monteCarlo.rmsePosition, 'r', 'LineWidth', 2);
xlabel('Time (s)');
ylabel('Position RMSE (m)');
legend('EKF', 'UKF');
set(gca, 'FontSize', 14);
grid on;

figure;
hold on;
plot(monteCarloEKF.monteCarlo.time, monteCarloEKF.monteCarlo.rmseAngle, 'LineWidth', 2);
plot(monteCarloUKF.monteCarlo.time, monteCarloUKF.monteCarlo.rmseAngle, 'r', 'LineWidth', 2);
xlabel('Time (s)');
ylabel('Angle RMSE (rad)');
legend('EKF', 'UKF');
set(gca, 'FontSize', 14);
grid on;

end