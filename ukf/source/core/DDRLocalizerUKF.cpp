//
// Created by mmaximo on 3/24/18.
//

#include "DDRLocalizerUKF.h"

#include "math.h"

#include "DDRModel.h"
#include "math/MathUtils.h"

UKFParams UKFParams::getDefaultUKFParams() {
    UKFParams params;
    params.alpha = 1.0e-3;
    params.beta = 2.0;
    params.kappa = 0.0;
    return params;
}

double UKFParams::getLambda(int L) {
    return alpha * alpha * (L + kappa) - L;
}

void UKFParams::getWeights(int L, double &wm0, double &wc0, double &wmi, double &wci) {
    double lambda = getLambda(L);
    wm0 = lambda / (L + lambda);
    wc0 = lambda / (L + lambda) + (1.0 - alpha * alpha + beta);
    wmi = wci = 1.0 / (2.0 * (L + lambda));
}

DDRLocalizerParams DDRLocalizerParams::getDefaultDDRLocalizerParams() {
    DDRLocalizerParams params;
    params.T = 1.0 / 30.0;
    params.sigmaLinearSpeed = 0.05;
    params.sigmaAngularSpeed = 0.02;
    params.sigmaCameraCartesian = 0.1;
    params.sigmaCameraAngle = 0.04;
    return params;
}

DDRLocalizerUKF::DDRLocalizerUKF(const DDRLocalizerParams &modelParams, const UKFParams &ukfParams) : modelParams(modelParams), ukfParams(ukfParams) {
    estimate.resize(3);
    covariance.resize(3, 3);
    M.resize(2, 2);
    M << modelParams.sigmaLinearSpeed * modelParams.sigmaLinearSpeed, 0.0,
        0.0, modelParams.sigmaAngularSpeed * modelParams.sigmaAngularSpeed;
    R.resize(3, 3);
    R << modelParams.sigmaCameraCartesian * modelParams.sigmaCameraCartesian, 0.0, 0.0,
        0.0, modelParams.sigmaCameraCartesian * modelParams.sigmaCameraCartesian, 0.0,
        0.0, 0.0, modelParams.sigmaCameraAngle * modelParams.sigmaCameraAngle;
    model = std::make_shared<DDRModel>(modelParams.T, M, R);
}

void DDRLocalizerUKF::reset(const math::Pose2D &pose, const Eigen::MatrixXd &covariance) {
    estimate(0) = pose.translation.x;
    estimate(1) = pose.translation.y;
    estimate(2) = pose.rotation;
    this->pose = pose;
    this->covariance = covariance;
}

const math::Pose2D &DDRLocalizerUKF::predict(double linearSpeed, double angularSpeed) {
    const int L = NUM_STATES + NUM_COMMANDS;
    const int NUM_SIGMA_POINTS = 2 * L + 1;
    static Eigen::VectorXd augmentedState(L);
    static Eigen::MatrixXd augmentedCovariance(L, L);
    Eigen::VectorXd sigmaPoints[NUM_SIGMA_POINTS];

    Eigen::VectorXd command(NUM_COMMANDS);
    command << linearSpeed, angularSpeed;
    augmentedState << estimate, Eigen::VectorXd::Zero(NUM_COMMANDS);
    augmentedCovariance.setZero();
    augmentedCovariance.topLeftCorner(covariance.rows(), covariance.cols()) = covariance;
    augmentedCovariance.bottomRightCorner(M.rows(), M.cols()) = M;
    drawSigmaPoints(augmentedState, augmentedCovariance, sigmaPoints, L);
    for (int i = 0; i <= 2 * L; ++i)
        sigmaPoints[i] = model->propagate(sigmaPoints[i].block(0, 0, NUM_STATES, 1), command + sigmaPoints[i].block(NUM_STATES, 0, NUM_COMMANDS, 1));
    computeStateMomentsFromSP(sigmaPoints, L, estimate, covariance);

    pose.translation.x = estimate(0);
    pose.translation.y = estimate(1);
    pose.rotation = estimate(2);
    return pose;
}

const math::Pose2D &DDRLocalizerUKF::filter(const math::Pose2D& observation) {
    const int L = NUM_STATES;
    const int NUM_SIGMA_POINTS = 2 * L + 1;
    Eigen::VectorXd stateSP[NUM_SIGMA_POINTS];
    Eigen::VectorXd observationSP[NUM_SIGMA_POINTS];

    Eigen::VectorXd z(3);
    z << observation.translation.x, observation.translation.y, observation.rotation;

    drawSigmaPoints(estimate, covariance, stateSP, L);
    for (int i = 0; i <= 2 * L; ++i)
        observationSP[i] = model->observe(stateSP[i]);

    Eigen::VectorXd stateMean(L);
    Eigen::MatrixXd stateCovariance(L, L);
    Eigen::VectorXd observationMean(NUM_OBSERVATIONS);
    Eigen::MatrixXd observationCovariance(NUM_OBSERVATIONS, NUM_OBSERVATIONS);
    Eigen::MatrixXd stateObservationCovariance(NUM_STATES, NUM_OBSERVATIONS);
    computeObservationMomentsFromSP(observationSP, L, observationMean, observationCovariance);
    computeStateObservationCovarianceFromSP(estimate, observationMean, stateSP, observationSP, L, stateObservationCovariance);

    Eigen::MatrixXd K = stateObservationCovariance * observationCovariance.inverse();
    Eigen::VectorXd observationError = z - observationMean;
    observationError(2) = math::MathUtils::normalizeAngle(observationError(2));
    estimate = estimate + K * observationError;
    covariance = covariance - K * observationCovariance * K.transpose();

    pose.translation.x = estimate(0);
    pose.translation.y = estimate(1);
    pose.rotation = estimate(2);
    return pose;
}

const Eigen::VectorXd &DDRLocalizerUKF::getState() {
    return estimate;
}

const math::Pose2D &DDRLocalizerUKF::getPose() {
    return pose;
}

const Eigen::MatrixXd &DDRLocalizerUKF::getCovariance() {
    return covariance;
}

void DDRLocalizerUKF::drawSigmaPoints(const Eigen::VectorXd &augmentedMean, const Eigen::MatrixXd &augmentedCovariance,
                                      Eigen::VectorXd *sigmaPoints, int L) {
    double lambda = ukfParams.getLambda(L);
    double gamma = std::sqrt(L + lambda);
    Eigen::MatrixXd F = augmentedCovariance.llt().matrixL();
    sigmaPoints[0] = augmentedMean;
    for (int i = 1; i <= L; ++i)
        sigmaPoints[i] = augmentedMean + gamma * F.block(0, i - 1, L, 1);
    for (int i = L + 1; i <= 2 * L; ++i)
        sigmaPoints[i] = augmentedMean - gamma * F.block(0, i - L - 1, L, 1);
}

void DDRLocalizerUKF::computeStateMomentsFromSP(const Eigen::VectorXd *sigmaPoints, int L, Eigen::VectorXd &mean,
                                                Eigen::MatrixXd &covariance) {

    double wm0, wc0, wmi, wci;
    ukfParams.getWeights(L, wm0, wc0, wmi, wci);
    mean(0) = wm0 * sigmaPoints[0](0);
    mean(1) = wm0 * sigmaPoints[0](1);
    double sumSin = wm0 * sin(sigmaPoints[0](2));
    double sumCos = wm0 * cos(sigmaPoints[0](2));
    for (int i = 1; i <= 2 * L; ++i) {
        mean(0) += wmi * sigmaPoints[i](0);
        mean(1) += wmi * sigmaPoints[i](1);
        sumSin += wmi * sin(sigmaPoints[i](2));
        sumCos += wmi * cos(sigmaPoints[i](2));
    }
    mean(2) = std::atan2(sumSin, sumCos);
    Eigen::VectorXd stateDiff = sigmaPoints[0].block(0, 0, NUM_STATES, 1) - mean;
    stateDiff(2) = math::MathUtils::normalizeAngle(stateDiff(2));
    covariance = wc0 * stateDiff * stateDiff.transpose();
    for (int i = 1; i <= 2 * L; ++i) {
        stateDiff = sigmaPoints[i].block(0, 0, NUM_STATES, 1) - mean;
        stateDiff(2) = math::MathUtils::normalizeAngle(stateDiff(2));
        covariance += wci * stateDiff * stateDiff.transpose();
    }
}

void DDRLocalizerUKF::computeObservationMomentsFromSP(const Eigen::VectorXd *sigmaPoints, int L, Eigen::VectorXd &mean,
                                                      Eigen::MatrixXd &covariance) {
    computeStateMomentsFromSP(sigmaPoints, L, mean, covariance);
    covariance += R;
}

void DDRLocalizerUKF::computeStateObservationCovarianceFromSP(const Eigen::VectorXd &stateMean,
                                                              const Eigen::VectorXd &observationMean,
                                                              const Eigen::VectorXd *stateSP, const Eigen::VectorXd *observationSP, int L,
                                                              Eigen::MatrixXd &covariance) {
    double wm0, wc0, wmi, wci;
    ukfParams.getWeights(L, wm0, wc0, wmi, wci);
    Eigen::VectorXd stateDiff = stateSP[0].block(0, 0, NUM_STATES, 1) - stateMean;
    stateDiff(2) = math::MathUtils::normalizeAngle(stateDiff(2));
    Eigen::VectorXd observationDiff = observationSP[0] - observationMean;
    observationDiff(2) = math::MathUtils::normalizeAngle(observationDiff(2));
    covariance = wc0 * stateDiff * observationDiff.transpose();
    for (int i = 1; i <= 2 * L; ++i) {
        stateDiff = stateSP[i].block(0, 0, NUM_STATES, 1) - stateMean;
        stateDiff(2) = math::MathUtils::normalizeAngle(stateDiff(2));
        observationDiff = observationSP[i] - observationMean;
        observationDiff(2) = math::MathUtils::normalizeAngle(observationDiff(2));
        covariance += wci * stateDiff * observationDiff.transpose();
    }
}
