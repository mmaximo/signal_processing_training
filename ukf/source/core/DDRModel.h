//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_DDRMODEL_H
#define EKF_DDRMODEL_H

#include "NonlinearModel.h"

/**
 * Represents a dynamical model for a differential-drive robot.
 *
 */
class DDRModel : public NonlinearModel {
public:
    /**
     * Constructs a dynamical model for a differential-drive robot.
     *
     * @param T sample time.
     * @param M command covariance.
     * @param R observation covariance.
     */
    DDRModel(double T, const Eigen::MatrixXd& M, const Eigen::MatrixXd& R);

    /**
     * Propagates the state using the motion model.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return new state after propagation.
     */
    Eigen::VectorXd propagate(const Eigen::VectorXd &state, const Eigen::VectorXd &command) const;

    /**
     * Computes a observation vector given the current state.
     *
     * @param state current state.
     * @return observation.
     */
    Eigen::VectorXd observe(const Eigen::VectorXd &state) const;

    /**
     * Obtains the jacobian matrix of the motion model with respect to the state.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return jacobian matrix.
     */
    Eigen::MatrixXd getA(const Eigen::VectorXd &state, const Eigen::VectorXd& command) const;

    /**
     * Obtains the jacobian matrix of the motion model with respect to the command.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return jacobian matrix.
     */
    Eigen::MatrixXd getB(const Eigen::VectorXd &state, const Eigen::VectorXd& command) const;

    /**
     * Obtains the jacobian matrix of the observation model with respect to the state.
     *
     * @param state current state.
     * @return jacobian matrix.
     */
    Eigen::MatrixXd getC(const Eigen::VectorXd &state) const;

    /**
     * Obtains the linearized covariance of the motion model.
     *
     * @param state current state.
     * @param command command executed by the robot.
     * @return linearized covariance of the motion model.
     */
    Eigen::MatrixXd getQ(const Eigen::VectorXd &state, const Eigen::VectorXd& command) const;

    /**
     * Obtains the covariance of the observation model.
     *
     * @param state current state.
     * @return covariance of the observation model.
     */
    Eigen::MatrixXd getR(const Eigen::VectorXd &state) const;

private:
    double T;
    static constexpr double EPSILON = 1.0e-3;
    Eigen::MatrixXd M;
    Eigen::MatrixXd R;
};


#endif //EKF_DDRMODEL_H
