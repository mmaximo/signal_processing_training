function testParticleFilterLocalizer()

% Run the component test
system('../binaries/ParticleFilterLocalizer_ComponentTest');

particles = load('particles.txt');
estimate = load('estimate.txt');
groundTruth = load('ground_truth.txt');
time = load('time.txt');
observationsFile = fopen('observations.txt');

epsilon = 0.1;

% set(gca, 'nextplot', 'replacechildren');

dt = (1.0 / 30.0) / 10.0;

arrowSize = 0.3;

figure;
hold on;
videoObject = VideoWriter('ParticleFilterLocalizer_ComponentTest.avi');
videoObject.FrameRate = 30;
videoObject.Quality = 100;
open(videoObject);
for i=1:size(estimate, 1)
    clf;
    hold on;
    plot(particles(i, 1:3:size(particles, 2)), particles(i, 2:3:size(particles, 2)), 'b.');
    drawArrow(estimate(i, 1:2), estimate(i, 1:2) + arrowSize * [cos(estimate(i, 3)), sin(estimate(i, 3))], 'r');
    drawArrow(groundTruth(i, 1:2), groundTruth(i, 1:2) + arrowSize * [cos(groundTruth(i, 3)), sin(estimate(i, 3))], 'g');
    line = fgetl(observationsFile);
    observations = strsplit(line, ' ');
    for j=1:length(observations)/2
        obsX = str2double(observations{2*(j-1)+1});
        obsY = str2double(observations{2*(j-1)+2});
        plot([groundTruth(i,1), obsX], [groundTruth(i,2), obsY], 'm');
    end
    range = max(max(max(abs(estimate(:,1:2)))), max(max(abs(groundTruth(:,1:2))))) + epsilon;
    axis([-(4/3)*range (4/3)*range -range range]);
%     axis([-15 15 -10 10]);
%     axis square;
    xlabel('X (m)');
    ylabel('Y (m)');
    grid on;
    drawnow;
    pause(dt);
    frame = getframe(gcf);
    writeVideo(videoObject, frame);
end
close(videoObject);

figure;
hold on;

plot(groundTruth(:, 1), groundTruth(:, 2), 'b');
plot(estimate(:, 1), estimate(:, 2), 'r');

xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;
axis equal

fprintf('Time statistics: mean: %f ms, std: %f ms\n', 1000 * mean(time), 1000 * std(time));

end