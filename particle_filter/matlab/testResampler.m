function testResampler()

% Run the component test
system('../binaries/Resampler_ComponentTest');

afterInitialize = load('after_initialize.txt');
afterFiltering = load('after_filtering.txt');
afterResample = load('after_resample.txt');

% Drawing particles after initialization and before filtering
drawParticles1D(afterInitialize(1, :), afterInitialize(2, :));
title('Particles - After Initialization');

% Drawing particles after filtering and before resampling
drawParticles1D(afterFiltering(1, :), afterFiltering(2, :));
title('Particles - After Filtering');

% Drawing particles after resampling
drawParticles1D(afterResample(1, :), afterResample(2, :));
title('Particles - After Resample');

end