function testObservationModel()

% Run the component test
system('../binaries/ObservationModel_ComponentTest');

particles = load('particle_and_weights.txt');

figure;
hold on;

% In this case, the particle weight is its 4th dimension
weights = particles(:, 4);
% To improve color visualization, we normalize particles by the maximum
% weight
weights = weights ./ max(weights);
numParticles = size(particles, 1);

for i=1:numParticles
    % Particles with higher weight are drawn in colors closer to red
    drawPose(particles(i, 1:3), 0.5, [weights(i), 0, 0]);
end

xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;

end