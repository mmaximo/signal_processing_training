function drawParticles1D(positions, weights)

figure;
hold on;
for i=1:length(positions)
    plot([positions(i), positions(i)],...
        [0, weights(i)]);
end

xlabel('Position (m)', 'FontSize', 14);
ylabel('Weight (-)', 'FontSize', 14);
set(gca, 'FontSize', 14);

end