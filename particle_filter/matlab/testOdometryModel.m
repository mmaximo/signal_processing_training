function testOdometryModel()

% Run the component test
system('../binaries/OdometryModel_ComponentTest');

particles = load('particles.txt');
groundTruth = load('ground_truth.txt');

numSteps = size(groundTruth, 1);

figure;
hold on;

% Drawing actual robot's pose
for k=1:numSteps
    drawPose(groundTruth(k, :), 0.2, 'r');
end

% Drawing particle set
for k=1:numSteps
    for i=1:size(particles, 2)/3
        particle = particles(k, 3*(i-1)+1:3*i);
        drawPose(particle, 0.1, 'b');
    end
end

xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;
axis equal;

end