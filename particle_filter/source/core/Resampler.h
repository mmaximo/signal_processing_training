//
// Created by mmaximo on 5/5/18.
//

#ifndef PARTICLE_FILTER_RESAMPLER_H
#define PARTICLE_FILTER_RESAMPLER_H

/**
 * Represents a O(N) resampler for a particle filter.
 */
class Resampler {
public:
    /**
     * Resamples a new particle set given particles' weights.
     *
     * @param weights particles' weights.
     * @param resampledIndices indices of the resampled particles.
     * @param numParticles number of particles in the particle set.
     */
    static void resample(double weights[], int resampledIndices[], int numParticles);
};


#endif //PARTICLE_FILTER_RESAMPLER_H
