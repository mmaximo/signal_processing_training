//
// Created by mmaximo on 3/24/18.
//

#include "MathUtils.h"
#include <cmath>

namespace math {

double MathUtils::normalizeAngle(double angle) {
    while (angle >= M_PI)
        angle -= 2.0 * M_PI;
    while (angle < -M_PI)
        angle += 2.0 * M_PI;
    return angle;
}

double MathUtils::degreesToRadians(double degrees) {
    return degrees * M_PI / 180.0;
}

double MathUtils::radiansToDegrees(double radians) {
    return radians * 180 / M_PI;
}

}