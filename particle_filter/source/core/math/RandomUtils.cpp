//
// Created by mmaximo on 3/16/18.
//

#include "RandomUtils.h"

#include <algorithm>

namespace math {

RandomUtils RandomUtils::instance;

double RandomUtils::generateGaussianRandomNumber(double mean, double sigma) {
    std::normal_distribution<double> gaussianDistribution(mean, sigma);
    return gaussianDistribution(getInstance().randomGenerator);
}

double RandomUtils::generateUniformRandomNumber(double min, double max) {
    std::uniform_real_distribution<double> uniformDistribution(min, max);
    return uniformDistribution(getInstance().randomGenerator);
}

int RandomUtils::generateIntegerRandomNumber(int min, int max) {
    std::uniform_int_distribution<int> integerDistribution(min, max);
    return integerDistribution(getInstance().randomGenerator);
}

void RandomUtils::shuffle(std::vector<int> &vector) {
    std::shuffle(vector.begin(), vector.end(), getInstance().randomGenerator);
}

RandomUtils::RandomUtils() {
    randomGenerator.seed(randomDevice());
}

RandomUtils &RandomUtils::getInstance() {
    return instance;
}

}