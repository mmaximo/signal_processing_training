//
// Created by mmaximo on 5/4/18.
//

#include "ObservationModel.h"

#include "math/MathUtils.h"

ObservationModel::ObservationModel(FieldDescription &fieldDescription, double sigmaDistance, double sigmaBearing)
        : fieldDescription(fieldDescription), sigmaDistance(sigmaDistance), sigmaBearing(sigmaBearing) {

}

double ObservationModel::computeLogProbabilityFromOneLandmark(const math::Pose2D &estimate,
                                                              const LandmarkObservation &observation,
                                                              const math::Vector2<double> &knownPosition) {
    double logProbability = 0.0;
    math::Vector2<double> differenceVector = knownPosition - estimate.translation;
    double distanceDifference = differenceVector.abs() - observation.getDistance();
    double bearingDifference = math::MathUtils::normalizeAngle(
            std::atan2(differenceVector.y, differenceVector.x) - estimate.rotation - observation.getBearing());
    logProbability = distanceDifference * distanceDifference / (sigmaDistance * sigmaDistance);
    logProbability += bearingDifference * bearingDifference / (sigmaBearing * sigmaBearing);
    return logProbability;
}

double ObservationModel::computeProbabilityFromLandmarks(const math::Pose2D &estimate,
                                                         const std::vector<GoalPostObservation> &goalPostObservations,
                                                         const std::vector<FlagObservation> &flagObservations) {
    double logProbability = 0.0;
    for (const GoalPostObservation &goalPostObservation : goalPostObservations)
        logProbability += computeLogProbabilityFromOneLandmark(estimate, goalPostObservation,
                                                               fieldDescription.getGoalPostKnownPosition(
                                                                       goalPostObservation.getGoalPostType()));
    for (const FlagObservation &flagObservation : flagObservations)
        logProbability += computeLogProbabilityFromOneLandmark(estimate, flagObservation,
                                                               fieldDescription.getFlagKnownPosition(
                                                                       flagObservation.getFlagType()));

    return std::exp(-logProbability);
}

