//
// Created by mmaximo on 5/5/18.
//

#include <math/MathUtils.h>
#include "ParticleFilterLocalizer.h"
#include "Resampler.h"

ParticleFilterLocalizerParams::ParticleFilterLocalizerParams() : sigmaDistance(0.0), sigmaBearing(0.0), sigmaVx(0.0),
                                                                 sigmaVy(0.0), sigmaVpsi(0.0), numParticles(200) {
}

ParticleFilterLocalizerParams ParticleFilterLocalizerParams::getDefaultParams() {
    ParticleFilterLocalizerParams params;
    params.sigmaDistance = 0.1;
    params.sigmaBearing = math::MathUtils::degreesToRadians(5.0);
    params.sigmaVx = 0.1;
    params.sigmaVy = 0.1;
    params.sigmaVpsi = math::MathUtils::degreesToRadians(10.0);
    params.numParticles = 300;
    params.alphaSlow = 0.01;
    params.alphaFast = 0.05;
    return params;
}

ParticleFilterLocalizer::ParticleFilterLocalizer(const ParticleFilterLocalizerParams &params,
                                                 FieldDescription &fieldDescription) : params(params),
                                                                                       odometryModel(
                                                                                               params.sigmaVx,
                                                                                               params.sigmaVy,
                                                                                               params.sigmaVpsi),
                                                                                       observationModel(
                                                                                               fieldDescription,
                                                                                               params.sigmaDistance,
                                                                                               params.sigmaBearing),
                                                                                       fieldDescription(
                                                                                               fieldDescription),
                                                                                       resetter(fieldDescription,
                                                                                                params.sigmaDistance,
                                                                                                params.sigmaBearing), slowObservationsQuality(1.0), fastObservationsQuality(1.0) {
    particles = new math::Pose2D[params.numParticles];
    newParticles = new math::Pose2D[params.numParticles];
    weights = new double[params.numParticles];
    resampledIndices = new int[params.numParticles];
    for (int i = 0; i < params.numParticles; ++i)
        weights[i] = 1.0 / params.numParticles;
}

ParticleFilterLocalizer::~ParticleFilterLocalizer() {
    delete[] particles;
    delete[] newParticles;
    delete[] weights;
    delete[] resampledIndices;
}


void
ParticleFilterLocalizer::reset(math::Pose2D &pose, math::Pose2D &sigmaPose) {
    resetter.resetParticleSetAroundEstimate(pose, sigmaPose, particles, params.numParticles);
    computeEstimate();
}

void ParticleFilterLocalizer::predict(double elapsedTime, const math::Pose2D &velocity) {
    for (int i = 0; i < params.numParticles; ++i) {
        particles[i] = odometryModel.predict(elapsedTime, particles[i], velocity);
    }
    computeEstimate();
}

void ParticleFilterLocalizer::filter(const std::vector<GoalPostObservation> &goalPostObservations,
                                     const std::vector<FlagObservation> &flagObservations) {
    updateWeights(goalPostObservations, flagObservations);
    resample();
    computeEstimate();
    sensorReset(goalPostObservations, flagObservations);
}

void ParticleFilterLocalizer::updateWeights(const std::vector<GoalPostObservation> &goalPostObservations,
                                            const std::vector<FlagObservation> &flagObservations) {
    double sumWeights = 0.0;
    double observationsQuality = 0.0;
    for (int i = 0; i < params.numParticles; ++i) {
        weights[i] = observationModel.computeProbabilityFromLandmarks(particles[i], goalPostObservations,
                                                                      flagObservations);
        sumWeights += weights[i];
        observationsQuality += weights[i];

    }
    for (int i = 0; i < params.numParticles; ++i) {
        weights[i] /= sumWeights;
    }
    if (observationsQuality < 1e-5) {
        slowObservationsQuality = 1.0;
        fastObservationsQuality = 0.0;
    } else {
        slowObservationsQuality = (1.0 - params.alphaSlow) * slowObservationsQuality + params.alphaSlow * observationsQuality;
        fastObservationsQuality = (1.0 - params.alphaFast) * fastObservationsQuality + params.alphaFast * observationsQuality;
    }
}

void ParticleFilterLocalizer::resample() {
    Resampler::resample(weights, resampledIndices, params.numParticles);
    for (int i = 0; i < params.numParticles; ++i) {
        newParticles[i] = particles[resampledIndices[i]];
        weights[i] = 1.0 / params.numParticles;
    }
    math::Pose2D *particlesAux = particles;
    particles = newParticles;
    newParticles = particlesAux;
}

void ParticleFilterLocalizer::sensorReset(const std::vector<GoalPostObservation> &goalPostObservations,
                                          const std::vector<FlagObservation> &flagObservations) {
    double resetProbability = std::max(0.0, 1.0 - fastObservationsQuality / slowObservationsQuality);
    resetter.sensorReset(particles, params.numParticles, goalPostObservations, flagObservations, resetProbability);
}

const math::Pose2D &ParticleFilterLocalizer::getEstimate() const {
    return estimate;
}

void ParticleFilterLocalizer::computeEstimate() {
    estimate.translation.x = 0.0;
    estimate.translation.y = 0.0;
    double sumSine = 0.0;
    double sumCossine = 0.0;
    for (int i = 0; i < params.numParticles; ++i) {
        estimate.translation.x += particles[i].translation.x;
        estimate.translation.y += particles[i].translation.y;
        sumSine += sin(particles[i].rotation);
        sumCossine += cos(particles[i].rotation);
    }
    estimate.translation.x /= params.numParticles;
    estimate.translation.y /= params.numParticles;
    estimate.rotation = std::atan2(sumSine, sumCossine);
}

const math::Pose2D *ParticleFilterLocalizer::getParticles() const {
    return particles;
}

const ParticleFilterLocalizerParams &ParticleFilterLocalizer::getParams() const {
    return params;
}


