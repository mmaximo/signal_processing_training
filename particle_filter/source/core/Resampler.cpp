//
// Created by mmaximo on 5/5/18.
//

#include "Resampler.h"
#include "math/RandomUtils.h"

void Resampler::resample(double weights[], int resampledIndices[], int numParticles) {
    int M = numParticles;
    double r = math::RandomUtils::generateUniformRandomNumber(0.0, 1.0 / M);
    double c = weights[0];
    int i = 0;
    for (int m = 0; m < M; ++m) {
        double U = r + static_cast<double>(m) / static_cast<double>(M);
        while (U > c) {
            ++i;
            c += weights[i];
        }
        resampledIndices[m] = i;
    }
}
