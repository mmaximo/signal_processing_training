/*
 * FieldDescription.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_FIELDDESCRIPTION_H_
#define SOURCE_MODELING_FIELDDESCRIPTION_H_

#include <string>
#include <map>

#include "PlaySide.h"
#include "FlagType.h"
#include "GoalPostType.h"
#include "math/Vector2.h"

/**
 * Class that represents the field description.
 */
class FieldDescription {
public:
    static const double FIELD_LENGTH;
    static const double FIELD_WIDTH;
    static const double SOCCER_2D_FIELD_LENGTH;
    static const double SOCCER_2D_FIELD_WIDTH;
    static const double GOAL_AREA_LENGTH;
    static const double GOAL_AREA_WIDTH;
    static const double PENALTY_AREA_LENGTH;

    static const double GOAL_POST_HEIGHT;
    static const double CENTER_CIRCLE_RADIUS;

    /**
     * Default constructor of FieldDescription.
     */

    FieldDescription();

    /**
     * Default destructor of FieldDescription.
     */

    virtual ~FieldDescription();

    /**
     * Updates playSide
     */
    void update(PlaySide::PLAY_SIDE playSide);

    /**
     * Gets the flag known position.
     *
     * @param flagType the type of the flag.
     *
     * @return The flag position.
     */
    const math::Vector2<double> &getFlagKnownPosition(FlagType::FLAG_TYPE flagType);

    /**
     * Gets the goal post know position.
     *
     * @param goalPostType the type of the goal post.
     *
     * @return the goal position.
     */

    const math::Vector2<double> &getGoalPostKnownPosition(GoalPostType::GOAL_POST_TYPE goalPostType);

    bool isPointWithinSoccerField(math::Vector2<double> point);

private:
    PlaySide::PLAY_SIDE currentPlaySide;

    math::Vector2<double> flagsPositionsLeft[FlagType::NUM_FLAG_TYPES];
    math::Vector2<double> goalPostsPositionsLeft[GoalPostType::NUM_GOAL_POST_TYPES];

    math::Vector2<double> flagsPositionsRight[FlagType::NUM_FLAG_TYPES];
    math::Vector2<double> goalPostsPositionsRight[GoalPostType::NUM_GOAL_POST_TYPES];
};


#endif /* SOURCE_MODELING_FIELDDESCRIPTION_H_ */
