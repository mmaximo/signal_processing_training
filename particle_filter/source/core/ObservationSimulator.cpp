//
// Created by mmaximo on 5/5/18.
//

#include "math/MathUtils.h"
#include "ObservationSimulator.h"

ObservationSimulator::ObservationSimulator(FieldDescription &fieldDescription, double sigmaDistance,
                                           double sigmaBearing, double fieldOfView) : fieldDescription(
        fieldDescription), sigmaDistance(sigmaDistance), sigmaBearing(sigmaBearing), fieldOfView(fieldOfView) {

}

void ObservationSimulator::observe(const math::Pose2D &pose, std::vector<GoalPostObservation> &goalPostsObservations,
                                   std::vector<FlagObservation> &flagObservations) {
    goalPostsObservations.clear();
    flagObservations.clear();
    math::Vector2<double> knownPosition;
    double distance, bearing;
    for (int i = 0; i < GoalPostType::NUM_GOAL_POST_TYPES; ++i) {
        auto type = static_cast<GoalPostType::GOAL_POST_TYPE>(i);
        knownPosition = fieldDescription.getGoalPostKnownPosition(type);
        bool isVisible = observePoint(pose, knownPosition, distance, bearing);
        if (isVisible)
            goalPostsObservations.push_back(GoalPostObservation(distance, bearing, type));
    }
    for (int i = 0; i < FlagType::NUM_FLAG_TYPES; ++i) {
        auto type = static_cast<FlagType::FLAG_TYPE>(i);
        knownPosition = fieldDescription.getFlagKnownPosition(type);
        bool isVisible = observePoint(pose, knownPosition, distance, bearing);
        if (isVisible)
            flagObservations.push_back(FlagObservation(distance, bearing, type));
    }
}

bool ObservationSimulator::observePoint(const math::Pose2D &pose, const math::Vector2<double> &point, double &distance, double &bearing) {
    math::Vector2<double> differenceVector = point - pose.translation;
    bearing = math::MathUtils::normalizeAngle(std::atan2(differenceVector.y, differenceVector.x) - pose.rotation);
    if (bearing >= -fieldOfView / 2.0 && bearing <= fieldOfView / 2.0) {
        distance = differenceVector.abs();
        return true;
    }
    return false;
}
