//
// Created by mmaximo on 5/4/18.
//

#include "LandmarkObservation.h"

LandmarkObservation::LandmarkObservation(double distance, double bearing) : distance(distance), bearing(bearing) {

}

double LandmarkObservation::getDistance() const {
    return distance;
}

double LandmarkObservation::getBearing() const {
    return bearing;
}

GoalPostObservation::GoalPostObservation(double distance, double bearing, GoalPostType::GOAL_POST_TYPE goalPostType)
        : LandmarkObservation(distance, bearing), goalPostType(goalPostType) {

}

GoalPostType::GOAL_POST_TYPE GoalPostObservation::getGoalPostType() const {
    return goalPostType;
}

FlagObservation::FlagObservation(double distance, double bearing, FlagType::FLAG_TYPE flagType) : LandmarkObservation(distance,
                                                                                                           bearing),
                                                                                       flagType(flagType) {

}

FlagType::FLAG_TYPE FlagObservation::getFlagType() const {
    return flagType;
}
