/*
 * PlaySide.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_PLAYSIDE_H_
#define SOURCE_REPRESENTATIONS_PLAYSIDE_H_

#include <iostream>

class PlaySide {
public:
    enum PLAY_SIDE {
        LEFT, RIGHT, UNKNOWN,
    };

    static std::string toString(PlaySide::PLAY_SIDE playSide) {
        std::string str;
        if (playSide == PlaySide::LEFT) {
            str = "Left";
            return str;
        } else if (playSide == PlaySide::RIGHT) {
            str = "Right";
            return str;
        } else {
            str = "Unknown";
            return str;
        }
    }

    static PlaySide::PLAY_SIDE getOppositePlaySide(PlaySide::PLAY_SIDE playSide) {
        if (playSide == PlaySide::LEFT) {
            return PlaySide::RIGHT;
        } else if (playSide == PlaySide::RIGHT) {
            return PlaySide::LEFT;
        } else {
            return PlaySide::UNKNOWN;
        }
    }
};

#endif /* SOURCE_REPRESENTATIONS_PLAYSIDE_H_ */
