//
// Created by mmaximo on 5/4/18.
//

#ifndef PARTICLE_FILTER_LANDMARKOBSERVATION_H
#define PARTICLE_FILTER_LANDMARKOBSERVATION_H

#include "math/Vector2.h"
#include "GoalPostType.h"
#include "FlagType.h"

/**
 * Represents a landmark observation.
 */
class LandmarkObservation {
public:
    /**
     * Constructs a landmark observation.
     *
     * @param distance observed distance to the landmark.
     * @param bearing observed bearing to the landmark.
     */
    LandmarkObservation(double distance, double bearing);

    /**
     * Obtains the observed distance from the robot to the landmark.
     *
     * @return distance to the landmark.
     */
    double getDistance() const ;

    /**
     * Obtains the observed bearing from the robot's front to the landmark.
     *
     * @return bearing to the landmark.
     */
    double getBearing() const ;

private:
    double distance;
    double bearing;
};

/**
 * Represents a goal post observation.
 */
class GoalPostObservation : public LandmarkObservation {
public:
    /**
     * Constructs a goal post observation.
     *
     * @param distance observed distance to the goal post.
     * @param bearing observed bearing to the goal post.
     * @param goalPostType type of the goal post.
     */
    GoalPostObservation(double distance, double bearing, GoalPostType::GOAL_POST_TYPE goalPostType);

    /**
     * Obtains the goal post type.
     *
     * @return type of the goal post.
     */
    GoalPostType::GOAL_POST_TYPE getGoalPostType() const ;

private:
    GoalPostType::GOAL_POST_TYPE goalPostType;
};

/**
 * Represents a flag observation.
 */
class FlagObservation : public LandmarkObservation {
public:
    /**
     * Constructs a flag observation.
     *
     * @param distance observed distance to the flag.
     * @param bearing observed bearing to the flag.
     * @param flagType type of the flag.
     */
    FlagObservation(double distance, double bearing, FlagType::FLAG_TYPE flagType);

    /**
     * Obtains the flag type.
     *
     * @return type of the flag.
     */
    FlagType::FLAG_TYPE getFlagType() const ;

private:
    FlagType::FLAG_TYPE flagType;
};

#endif //PARTICLE_FILTER_LANDMARKOBSERVATION_H
