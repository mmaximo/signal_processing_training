//
// Created by mmaximo on 5/5/18.
//

#ifndef PARTICLE_FILTER_OBSERVATIONSIMULATOR_H
#define PARTICLE_FILTER_OBSERVATIONSIMULATOR_H

#include "math/Pose2D.h"
#include "FieldDescription.h"
#include "LandmarkObservation.h"
#include <vector>

/**
 * Represents a simulator for robot soccer landmark-based observation.
 */
class ObservationSimulator {
public:
    /**
     * Constructs a robot soccer observation simulator.
     *
     * @param fieldDescription description of the soccer field.
     * @param sigmaDistance standard deviation of distance observations.
     * @param sigmaBearing standard deviation of bearing observations.
     * @param fieldOfView robot's field of view.
     */
    ObservationSimulator(FieldDescription &fieldDescription, double sigmaDistance, double sigmaBearing, double fieldOfView);

    /**
     * Observes landmarks in the soccer field.
     *
     * @param pose current robot's pose.
     * @param goalPostsObservations goal post observations.
     * @param flagObservations flag observations.
     */
    void observe(const math::Pose2D &pose, std::vector<GoalPostObservation> &goalPostsObservations,
                 std::vector<FlagObservation> &flagObservations);

private:
    bool observePoint(const math::Pose2D &pose, const math::Vector2<double> &point, double &distance, double &bearing);

    FieldDescription &fieldDescription;
    double sigmaDistance;
    double sigmaBearing;
    double fieldOfView;
};


#endif //PARTICLE_FILTER_OBSERVATIONSIMULATOR_H
