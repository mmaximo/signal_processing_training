//
// Created by mmaximo on 5/4/18.
//

#ifndef PARTICLE_FILTER_ODOMETRYMODEL_H
#define PARTICLE_FILTER_ODOMETRYMODEL_H

#include "math/Pose2D.h"

/**
 * Represents a odometry model for an omnidirectional robot.
 */
class OdometryModel {
public:
    /**
     * Constructs a odometry model for an omnidirectional robot.
     *
     * @param sigmaVx standard deviation of frontal speed.
     * @param sigmaVy standard deviation of sideways speed.
     * @param sigmaVpsi standard deviation of rotational speed.
     */
    OdometryModel(double sigmaVx, double sigmaVy, double sigmaVpsi);

    /**
     * Predicts next pose using odometry.
     *
     * @param elapsedTime elapsed time since last pose update.
     * @param pose last pose.
     * @param velocity robot's commanded velocity.
     * @return new pose.
     */
    math::Pose2D predict(double elapsedTime, const math::Pose2D &pose, const math::Pose2D &velocity);

private:
    double sigmaVx;
    double sigmaVy;
    double sigmaVpsi;
};


#endif //PARTICLE_FILTER_ODOMETRYMODEL_H
