//
// Created by mmaximo on 5/5/18.
//

#include "Resetter.h"
#include "OdometryModel.h"
#include "math/Pose2D.h"
#include "math/MathUtils.h"
#include <fstream>

int main() {
    std::ofstream particlesFile("particles.txt");
    std::ofstream groundTruthFile("ground_truth.txt");

    using math::MathUtils;
    using math::Pose2D;

    // Parameters
    double T = 1.0;
    double sigmaVx = 0.05;
    double sigmaVy = 0.05;
    double sigmaVpsi = MathUtils::degreesToRadians(3.0);
    double sigmaDistance = 0.1;
    double sigmaBearing = MathUtils::degreesToRadians(10.0);

    Pose2D pose(0.0, 0.0, 0.0);
    Pose2D velocity;
    OdometryModel model(sigmaVx, sigmaVy, sigmaVpsi);

    const int NUM_PARTICLES = 30;
    Pose2D particles[NUM_PARTICLES];

    FieldDescription fieldDescription;
    Resetter resetter(fieldDescription, sigmaDistance, sigmaBearing);

    math::Pose2D sigmaInitialPose(MathUtils::degreesToRadians(1.0), 0.1, 0.1);
    resetter.resetParticleSetAroundEstimate(pose, sigmaInitialPose, particles, NUM_PARTICLES);

    int numSteps = 15;
    for (int k = 0; k < numSteps; ++k) {
        if (k == numSteps / 3 || k == 2 * numSteps / 3)
            velocity = Pose2D(MathUtils::degreesToRadians(90.0), 0.0, 0.0); // rotate 90 degrees
        else
            velocity = Pose2D(0.0, 1.0, 0.0); // go forward
        // Propagate particle set
        for (int i = 0; i < NUM_PARTICLES; ++i) {
            particles[i] = model.predict(T, particles[i], velocity);
            particlesFile << particles[i].translation.x << " " << particles[i].translation.y << " " << particles[i].rotation << " ";
        }
        particlesFile << std::endl;
        // Propagate actual robot's pose
        pose = model.predict(T, pose, velocity);
        groundTruthFile << pose.translation.x << " " << pose.translation.y << " " << pose.rotation << std::endl;
    }
    particlesFile.flush();
    groundTruthFile.flush();
    particlesFile.close();
    groundTruthFile.close();
}