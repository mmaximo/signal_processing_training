//
// Created by mmaximo on 03/05/16.
//

#include "Resampler.h"
#include <cmath>
#include <fstream>

/**
 * Writes particles in a file.
 *
 * @param file where particles will be written.
 * @param positions particles' positions.
 * @param weights particles' weights.
 * @param numParticles number of particles.
 */
void printParticles(std::ofstream &file, double *positions, double *weights, int numParticles) {
    for (int i = 0; i < numParticles; ++i) {
        file << positions[i] << " ";
    }
    file << std::endl;
    for (int i = 0; i < numParticles; ++i) {
        file << weights[i] << " ";
    }
    file << std::endl;
    file.flush();
}

int main() {
    std::ofstream afterInitialize("after_initialize.txt");
    std::ofstream afterFiltering("after_filtering.txt");
    std::ofstream afterResample("after_resample.txt");

    int numParticles = 500;

    // Door parameters
    double doorMean1 = 3.0;
    double doorMean2 = 4.0;
    double doorMean3 = 7.0;
    double doorSigma = 0.2;

    double minPosition = 0.0;
    double maxPosition = 10.0;
    // Filter variables
    double particlesPositions[numParticles];
    double newParticlesPositions[numParticles];
    double particlesWeights[numParticles];
    int resampledIndices[numParticles];

    // Initialization
    for (int i = 0; i < numParticles; ++i) {
        // Particles equally distributed in space
        particlesPositions[i] = minPosition + i * (maxPosition - minPosition) / numParticles;
        particlesWeights[i] = 1.0 / numParticles;
    }
    printParticles(afterInitialize, particlesPositions, particlesWeights, numParticles);

    double sumWeights = 0.0;
    for (int i = 0; i < numParticles; ++i) {
        double doorDiff1 = particlesPositions[i] - doorMean1;
        double doorDiff2 = particlesPositions[i] - doorMean2;
        double doorDiff3 = particlesPositions[i] - doorMean3;
        // Observation model
        particlesWeights[i] = exp(
                -(doorDiff1 * doorDiff1) / (doorSigma * doorSigma)) +
                              exp(-(doorDiff2 * doorDiff2) / (doorSigma * doorSigma)) +
                              exp(-(doorDiff3 * doorDiff3) / (doorSigma * doorSigma));
        sumWeights += particlesWeights[i];
    }
    // Normalizing the weights
    for (int i = 0; i < numParticles; ++i) {
        particlesWeights[i] /= sumWeights;
    }

    printParticles(afterFiltering, particlesPositions, particlesWeights, numParticles);

    // Resampling particles
    Resampler::resample(particlesWeights, resampledIndices, numParticles);

    for (int i = 0; i < numParticles; ++i) {
        newParticlesPositions[i] = particlesPositions[resampledIndices[i]];
        particlesWeights[i] = 1.0 / numParticles;
    }

    printParticles(afterResample, newParticlesPositions, particlesWeights, numParticles);
}